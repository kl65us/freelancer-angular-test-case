import angular from 'angular';

import {hello} from './app/hello';
import {login} from './login/login';
import {authService} from './services/auth.service';
import {messageService} from './services/message.service';
import 'angular-ui-router';
import routesConfig from './routes';

import './index.scss';

export const app = 'app';

angular
  .module(app, ['ui.router'])
  .config(routesConfig)
  .factory('AuthService', authService)
  .factory('MessageService', messageService)
  .component('app', hello)
  .component('login', login);
