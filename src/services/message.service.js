export const messageService = function () {
  const messages = [];
  const audio = new Audio('sounds/bell-ding.mp3');
  audio.loop = false;

  function getMessages() {
    return messages;
  }

  function sendMessage(author, message, playNotification = false) {
    messages.push({
      author,
      message,
      timestamp: (new Date()).getTime()
    });

    if (playNotification) {
      audio.play();
    }
  }

  return {
    getMessages,
    sendMessage
  };
};
