export const authService = function () {
  function logOut() {
    return new Promise(resolve => {
      localStorage.removeItem('userName');
      resolve('Success');
    });
  }

  function login(userName) {
    return new Promise((resolve, reject) => {
      if (!userName) {
        return reject('Username is not defined');
      }

      localStorage.setItem('userName', userName);
      resolve('Success');
    });
  }

  function isSignedIn() {
    return Boolean(localStorage.getItem('userName'));
  }

  return {
    logOut,
    login,
    isSignedIn
  };
};
