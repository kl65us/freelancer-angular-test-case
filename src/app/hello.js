import angular from 'angular';

export const hello = {
  template: require('./hello.html'),
  controller($scope, $state, $interval, $http, AuthService, MessageService) {
    const vm = this;

    if (!AuthService.isSignedIn()) {
      return $state.go('login');
    }

    this.userName = localStorage.getItem('userName');
    this.message = '';
    this.messages = MessageService.getMessages();
    this.testMessages = [];

    this.logOut = function () {
      AuthService
        .logOut()
        .then(() => {
          $state.go('login');
        });
    };

    this.sendMessage = function () {
      MessageService.sendMessage(this.userName, this.message);
      this.message = '';
    };

    const interval = $interval(() => {
      const index = Math.floor(Math.random() * vm.testMessages.length);
      const message = angular.copy(vm.testMessages[index]);

      MessageService.sendMessage(message.author, message.message, true);
    }, 4000);

    $http
      .get('messages.json')
      .then(res => {
        vm.testMessages = res.data;
      });

    $scope.$on('$destroy', () => {
      $interval.cancel(interval);
    });
  }
};
