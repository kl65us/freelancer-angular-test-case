export const login = {
  template: require('./login.html'),
  controller($state, AuthService) {
    this.userName = '';

    this.doLogin = function () {
      AuthService
        .login(this.userName)
        .then(() => {
          $state.go('app');
        });
    };
  }
};

